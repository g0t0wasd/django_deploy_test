from django.db import models

# Create your models here.

class Blog(models.Model):
    title = models.CharField(max_length=100, unique=True)
    body = models.TextField()
    img = models.ImageField(null=True, blank=True, upload_to="uploads/")

    def __str__(self):
        return self.title
