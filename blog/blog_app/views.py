from django.shortcuts import render_to_response
from blog_app.models import Blog
# Create your views here.

def index(request):
    blogs = Blog.objects.all()
    return render_to_response('index.html', {'blogs': blogs})
